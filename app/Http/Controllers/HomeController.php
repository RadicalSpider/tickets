<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    public function getTickets(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nombre'=>'required',
            'email'=>'required|email|unique:tickets',
            'boletos'=>'required|min:1|max:10',
        ]);

        $ticket = new Ticket;
        $ticket->nombre = $request->nombre;
        $ticket->email = $request->email;
        $ticket->boletos = $request->boletos;

        if($ticket->save())
        {
            return redirect()->route('home')->with('msg', 'Hemos recibido tu petición en breve te contactaremos...');
        }

        return redirect()->route('home')->with('msg', 'Hubo un problema con tu petición, intentalo nuevamente...');
    }
}
