<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Concert;

class ConcertController extends Controller
{
    public function index()
    {
    	$concerts = Concert::All();

    	return view('admin.concert.index', compact('concerts'));
    }
}
