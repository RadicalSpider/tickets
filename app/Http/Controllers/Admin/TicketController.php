<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;

class TicketController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
    	$tickets = Ticket::all();

    	return view('admin.ticket.index', compact('tickets'));
    }
}
