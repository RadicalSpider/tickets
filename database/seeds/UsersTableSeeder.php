<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
        	'name' => 'Daniel Lucas',
        	'email' => 'danielks@correo.com',
        	'rol' => 'admin',
        	'password' => bcrypt('123456')
        ]);
    }
}
