<div class="card card-concert">

	<img class="card-img-top" src="{{ asset('images/concierto.jpg') }}" alt="Concierto">

	<div class="card-body">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		{!! Form::open(["route" => "get-tickets"]) !!}
			<div class="form-group">
				{{ Form::text('nombre', null, ['placeholder'=>'Ingresa tu nombre...', 'class' => 'form-control']) }}
			</div>

			<div class="form-group">
				{{ Form::email('email', null, ['placeholder'=>'Ingresa tu correo...', 'class' => 'form-control']) }}
			</div>
			
			<div class="form-group">
				{{ Form::number('boletos', 1, ['min' => 1, 'max' => 10, 'class' => 'form-control']) }}
			</div>

			<div class="form-group">
				<input type="submit" value="Solicitar boletos" class="btn btn-warning btn-block text-uppercase">
			</div>
		{!! Form::close() !!}
	</div>
</div>