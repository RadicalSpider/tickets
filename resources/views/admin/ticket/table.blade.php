<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Id</th>
			<th>Nombre</th>
			<th>Email</th>
			<th>Boletos</th>
			<th>Registro</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($tickets as $ticket)
			<tr>
				<td>{{ $loop->index + 1 }}</td>
				<td>{{ $ticket->nombre }}</td>
				<td>{{ $ticket->email }}</td>
				<td>{{ $ticket->boletos }}</td>
				<td>{{ $ticket->created_at->diffForHumans() }}</td>
				<td>Editar | Eliminar</td>
			</tr>
		@empty
			<tr>
				<td colspan="6">
					No hay solicitudes de boletos registradas...							
				</td>
			</tr>
		@endforelse
	</tbody>
</table>