@extends('layouts.admin')

@section('content')
	<div class='container'>
		<h1>Tickets Admin.</h1><hr>

		@include('admin.ticket.table')
	</div>
@endsection