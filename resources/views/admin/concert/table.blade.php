<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Poster</th>
			<th>Título</th>
			<th>Activo</th>
			<th>Creado</th>
			<th>Actualizado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($concerts as $concert)
			<tr>
				<td>{{ $loop->index + 1 }}</td>
				<td>{{ $ticket->poster }}</td>
				<td>{{ $ticket->title }}</td>
				<td>{{ $ticket->active }}</td>
				<td>{{ $ticket->created_at->diffForHumans() }}</td>
				<td>{{ $ticket->updated_at->diffForHumans() }}</td>
				<td>Editar | Eliminar</td>
			</tr>
		@empty
			<tr>
				<td colspan="7">
					No hay conciertos registrados...							
				</td>
			</tr>
		@endforelse
	</tbody>
</table>