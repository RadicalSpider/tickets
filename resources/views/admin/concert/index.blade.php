@extends('layouts.admin')

@section('content')
	<div class='container'>
		<h1>Concerts Admin.</h1><hr>

		@include ('admin.concert.table')
	</div>
@endsection