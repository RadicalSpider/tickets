<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('get-tickets', 'HomeController@getTickets')->name('get-tickets');

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin', 'namespace' => 'Admin'], function(){
	Route::get('tickets', 'TicketController@index')->name('tickets.index');
	Route::get('concerts', 'ConcertController@index')->name('concerts.index');
});